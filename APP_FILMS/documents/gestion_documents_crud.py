"""
    Fichier : gestion_documents_crud.py
    Auteur : FG 2021.04.27
    Gestions des "routes" FLASK et des données pour les genres.
"""
import sys
# Flo > Il faut OS
import os

import pymysql
from flask import flash
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for
from flask import send_from_directory

from werkzeug.utils import secure_filename
# Flo > Import de trucs de signatures de fichiers
# constants
from APP_FILMS.pyfsig.file_signatures import signatures
# classes
from APP_FILMS.pyfsig.file_signatures import Signature, Matches, NoMatchException
# defs
from APP_FILMS.pyfsig.file_signatures import compare_sig, get_from_file, get_from_path
# Flo ---------------------------------------
from APP_FILMS import obj_mon_application
from APP_FILMS.database.connect_db_context_manager import MaBaseDeDonnee
from APP_FILMS.database.database_tools import Toolsbd
from APP_FILMS.erreurs.exceptions import *
from APP_FILMS.erreurs.msg_erreurs import *
from APP_FILMS.documents.gestion_documents_wtf_forms import FormWTFAjouterDocuments
from APP_FILMS.documents.gestion_documents_wtf_forms import FormWTFDeleteDocument
from APP_FILMS.documents.gestion_documents_wtf_forms import FormWTFUpdateDocument

"""
    Auteur : OM 2021.03.16
    Définition d'une "route" /documents_afficher
    
    Test : ex : http://127.0.0.1:5005/documents_afficher
    
    Paramètres : order_by : ASC : Ascendant, DESC : Descendant
                id_genre_sel = 0 >> tous les genres.
                id_genre_sel = "n" affiche le genre dont l'id est "n"
"""


@obj_mon_application.route("/documents_afficher/<string:order_by>/<int:id_genre_sel>", methods=['GET', 'POST'])
def documents_afficher(order_by, id_genre_sel):
    if request.method == "GET":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion genres ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur GestionGenres {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                if order_by == "ASC" and id_genre_sel == 0:
                    strsql_genres_afficher = """SELECT id_document, docName, docURI, docDesc FROM t_document ORDER BY id_document ASC"""
                    mc_afficher.execute(strsql_genres_afficher)
                elif order_by == "ASC":
                    # C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
                    # la commande MySql classique est "SELECT * FROM t_genre"
                    # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
                    # donc, je précise les champs à afficher
                    # Constitution d'un dictionnaire pour associer l'id du genre sélectionné avec un nom de variable
                    valeur_id_genre_selected_dictionnaire = {"value_id_genre_selected": id_genre_sel}
                    strsql_genres_afficher = """SELECT id_document, docName, docURI, docDesc FROM t_document WHERE id_document = %(value_id_genre_selected)s"""

                    mc_afficher.execute(strsql_genres_afficher, valeur_id_genre_selected_dictionnaire)
                else:
                    strsql_genres_afficher = """SELECT id_document, docName, docURI, docDesc FROM t_document ORDER BY id_document DESC"""

                    mc_afficher.execute(strsql_genres_afficher)

                data_genres = mc_afficher.fetchall()

                print("data_genres ", data_genres, " Type : ", type(data_genres))

                # Différencier les messages si la table est vide.
                if not data_genres and id_genre_sel == 0:
                    flash("""La table "t_document" est vide. !!""", "warning")
                elif not data_genres and id_genre_sel > 0:
                    # Si l'utilisateur change l'id_genre dans l'URL et que le genre n'existe pas,
                    flash(f"Le document demandé n'existe pas !!", "warning")
                else:
                    # Dans tous les autres cas, c'est que la table "t_genre" est vide.
                    # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                    flash(f"Données documents affichées !!", "success")

        except Exception as erreur:
            print(f"RGG Erreur générale. documents_afficher")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)"
            # fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            flash(f"RGG Exception {erreur} documents_afficher", "danger")
            raise Exception(f"RGG Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # Envoie la page "HTML" au serveur.
    return render_template("documents/documents_afficher.html", data=data_genres)


"""
    Auteur : OM 2021.03.22
    Définition d'une "route" /genres_ajouter
    
    Test : ex : http://127.0.0.1:5005/genres_ajouter
    
    Paramètres : sans
    
    But : Ajouter un genre pour un film
    
    Remarque :  Dans le champ "name_genre_html" du formulaire "genres/genres_ajouter.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/documents_ajouter", methods=['GET', 'POST'])
def documents_ajouter_wtf():
    type_selectionne = None
    form = FormWTFAjouterDocuments()
    if request.method == "GET":
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                strsql_type_afficher = """SELECT id_type, fileType FROM t_type ORDER BY id_type ASC"""
                mc_afficher.execute(strsql_type_afficher)

            data_type = mc_afficher.fetchall()
            print("Flo > data_type ", data_type, " Type : ", type(data_type))

            #type_val_list_dropdown = []
            #for i in data_type:
                #type_val_list_dropdown.append(i['fileType'])

            # Aussi possible d'avoir un id numérique et un texte en correspondance

            type_val_list_dropdown = [("", "--------------")]+[(i['id_type'], i['fileType']) for i in data_type]

            print("type_val_list_dropdown ", type_val_list_dropdown)

            form.type_document_wtf.choices = type_val_list_dropdown
            session['type_val_list_dropdown'] = type_val_list_dropdown
            # Ceci est simplement une petite démo. on fixe la valeur PRESELECTIONNEE de la liste
            form.type_document_wtf.data = None
            type_selectionne = form.type_document_wtf.data
            print("type choisi dans la liste :", type_selectionne)
            session['type_selectionne_get'] = type_selectionne
    if request.method == "POST":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion genres ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur GestionGenres {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            if form.submit.data:
                type_selectionne = form.type_document_wtf.data
                print("Type sélectionné : ",
                      type_selectionne)
                form.type_document_wtf.choices = session['type_val_list_dropdown']

            if form.validate_on_submit():
                print("Formulaire envoyé")
                nom_document_wtf = form.nom_document_wtf.data
                desc_document_wtf = form.desc_document_wtf.data
                url_document_wtf = form.url_document_wtf.data
                type_document_wtf = form.type_document_wtf.data

                name_document = nom_document_wtf
                desc_document = desc_document_wtf
                url_document = url_document_wtf
                type_document = type_document_wtf
                print('Flo N.Type document : ', type_document)

                uploaded_file = form.file_document_wtf.data
                print(uploaded_file)
                if uploaded_file != None:
                    print(uploaded_file.filename)
                    #filename = uploaded_file.filename
                    #Flo une méthode de werkzeug pour sécuriser un peu plus
                    filename = secure_filename(uploaded_file.filename)
                    print(filename)
                    if filename != '':
                        # Flo > .PDF retourne .pdf
                        file_ext_check = os.path.splitext(filename)[1].lower()
                        #retourne pdf
                        file_ext = os.path.splitext(filename)[1][1:].strip().lower()
                        print(file_ext)
                        #Flo > Check si l'extension fait partie des extensions autorisée dans __init__
                        if file_ext_check not in obj_mon_application.config['UPLOAD_EXTENSIONS']:
                            return "Document invalide", 400


                        #Flo > Check en BDD pour récuperer l'ID Type par rapport à l'extension de fichier
                        objet_connectbd = Toolsbd()
                        connect_mabd = objet_connectbd.connect_database()
                        curseur_mabd = connect_mabd.cursor()

                        curseur_mabd.execute('SELECT * FROM t_type WHERE fileType = %s', (file_ext,))
                        checktype = curseur_mabd.fetchone()

                        if checktype:
                            id_type_fichier = checktype['id_type']
                            checktype = checktype['fileSignature']
                        else:
                            return "Vérification ce type de fichier n'est pas autorisé en BD Flo", 400

                        curseur_mabd.execute('SELECT * FROM t_type WHERE fileType = %s', (file_ext,))


                        print('Flo > Id Type fichier BDD', id_type_fichier)
                        print('Type trouvé BDD : ',checktype)

                        uploaded_file.save(os.path.join('APP_FILMS',obj_mon_application.config['UPLOAD_PATH'], filename))
                        flash('Document envoyé avec succes')
                        #Flo > Enregistre le chemin local
                        url_document = os.path.join(obj_mon_application.config['UPLOAD_PATH'], filename)
                        type_document = id_type_fichier
                        print(url_document)
                        #Test signature
                        fullpath = os.path.abspath(os.path.join('APP_FILMS',obj_mon_application.config['UPLOAD_PATH'], filename))
                        print(fullpath)
                        typedefichier = get_from_path(fullpath)
                        print(typedefichier)
                        for types in typedefichier:
                             if file_ext == types['file_extension']:
                                 print('TYPE TROUVE! ', types['file_extension'])
                                 print('Message de FLO : envoyé avec succès')
                                 #return redirect(url_for('upload'))
                                 break
                             else:
                                return render_template('hack.html', content=typedefichier, check=checktype, fileext=file_ext)


                valeurs_insertion_dictionnaire = {"value_name_document": name_document, "value_desc_document": desc_document,
                                                  "value_url_document": url_document,
                                                  "value_type_document": type_document}
                print("valeurs_insertion_dictionnaire ", valeurs_insertion_dictionnaire)

                strsql_insert_genre = """INSERT INTO t_document (id_document,docName,docDesc,docURI,fk_type) VALUES (NULL,%(value_name_document)s,%(value_desc_document)s,%(value_url_document)s,%(value_type_document)s)"""
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(strsql_insert_genre, valeurs_insertion_dictionnaire)

                flash(f"Données insérées !!", "success")
                print(f"Données insérées !!")

                # Pour afficher et constater l'insertion de la valeur, on affiche en ordre inverse. (DESC)
                #return redirect(url_for('documents_afficher', order_by='DESC', id_genre_sel=0))
                return redirect(url_for('films_genres_afficher', id_film_sel=0))

        # ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur_genre_doublon:
            # Dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs/exceptions.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            code, msg = erreur_genre_doublon.args

            flash(f"{error_codes.get(code, msg)} ", "warning")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur_gest_genr_crud:
            code, msg = erreur_gest_genr_crud.args

            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Erreur dans Gestion genres CRUD : {sys.exc_info()[0]} "
                  f"{erreur_gest_genr_crud.args[0]} , "
                  f"{erreur_gest_genr_crud}", "danger")

    return render_template("documents/documents_ajouter_wtf.html", form=form)


"""
    Auteur : OM 2021.03.29
    Définition d'une "route" /genre_update
    
    Test : ex cliquer sur le menu "genres" puis cliquer sur le bouton "EDIT" d'un "genre"
    
    Paramètres : sans
    
    But : Editer(update) un genre qui a été sélectionné dans le formulaire "genres_afficher.html"
    
    Remarque :  Dans le champ "nom_genre_update_wtf" du formulaire "genres/genre_update_wtf.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/document_update", methods=['GET', 'POST'])
def document_update_wtf():

    # L'utilisateur vient de cliquer sur le bouton "EDIT". Récupère la valeur de "id_genre"
    id_doc_update = request.values['id_genre_btn_edit_html']

    # Objet formulaire pour l'UPDATE
    form_update = FormWTFUpdateDocument()
    try:
        print(" on submit ", form_update.validate_on_submit())
        if form_update.validate_on_submit():
            # Récupèrer la valeur du champ depuis "genre_update_wtf.html" après avoir cliqué sur "SUBMIT".
            # Puis la convertir en lettres minuscules.
            nom_document_update = form_update.nom_document_update_wtf.data
            # name_genre_update = name_genre_update.lower()
            url_document_update = form_update.url_document_update_wtf.data
            desc_document_update = form_update.desc_document_update_wtf.data

            valeur_update_dictionnaire = {"value_id_doc": id_doc_update, "value_name_doc": nom_document_update,
                                          "value_url_doc": url_document_update, "value_desc_doc": desc_document_update}
            print("valeur_update_dictionnaire ", valeur_update_dictionnaire)

            str_sql_update_intitulegenre = """UPDATE t_document SET docName = %(value_name_doc)s, docURI = %(value_url_doc)s, docDesc = %(value_desc_doc)s WHERE id_document = %(value_id_doc)s"""
            print('Données UPDATED correctement #Flo')
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(str_sql_update_intitulegenre, valeur_update_dictionnaire)

            flash(f"Donnée mise à jour !!", "success")
            print(f"Donnée mise à jour !!")

            # afficher et constater que la donnée est mise à jour.
            # Affiche seulement la valeur modifiée, "ASC" et l'"id_genre_update"
            #return redirect(url_for('documents_afficher', order_by="ASC", id_genre_sel=id_doc_update))
            return redirect(url_for('films_genres_afficher', id_film_sel=id_doc_update))
        elif request.method == "GET":
            # Opération sur la BD pour récupérer "id_genre" et "intitule_genre" de la "t_genre"
            str_sql_id_genre = "SELECT id_document, docName, docURI, docDesc FROM t_document WHERE id_document = %(value_id_doc)s"
            valeur_select_dictionnaire = {"value_id_doc": id_doc_update}
            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()
            mybd_curseur.execute(str_sql_id_genre, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()", vu qu'il n'y a qu'un seul champ "nom genre" pour l'UPDATE
            data_nom_genre = mybd_curseur.fetchone()
            print("data_nom_genre ", data_nom_genre, " type ", type(data_nom_genre), " genre ",
                  data_nom_genre["docName"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "genre_update_wtf.html"
            form_update.nom_document_update_wtf.data = data_nom_genre["docName"]
            form_update.url_document_update_wtf.data = data_nom_genre["docURI"]
            form_update.desc_document_update_wtf.data = data_nom_genre["docDesc"]

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans document_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans document_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")
        flash(f"Erreur dans document_update_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")
        flash(f"__KeyError dans document_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("documents/document_update_wtf.html", form_update=form_update)


"""
    Auteur : OM 2021.04.08
    Définition d'une "route" /genre_delete
    
    Test : ex. cliquer sur le menu "genres" puis cliquer sur le bouton "DELETE" d'un "genre"
    
    Paramètres : sans
    
    But : Effacer(delete) un genre qui a été sélectionné dans le formulaire "genres_afficher.html"
    
    Remarque :  Dans le champ "nom_genre_delete_wtf" du formulaire "genres/genre_delete_wtf.html",
                le contrôle de la saisie est désactivée. On doit simplement cliquer sur "DELETE"
"""


@obj_mon_application.route("/document_delete", methods=['GET', 'POST'])
def document_delete_wtf():
    data_films_attribue_genre_delete = None
    btn_submit_del = None
    # L'utilisateur vient de cliquer sur le bouton "DELETE". Récupère la valeur de "id_genre"
    id_genre_delete = request.values['id_genre_btn_delete_html']

    # Objet formulaire pour effacer le genre sélectionné.
    form_delete = FormWTFDeleteDocument()
    try:
        print(" on submit ", form_delete.validate_on_submit())
        if request.method == "POST" and form_delete.validate_on_submit():

            if form_delete.submit_btn_annuler.data:
                #return redirect(url_for("documents_afficher", order_by="ASC", id_genre_sel=0))
                return redirect(url_for('films_genres_afficher', id_film_sel=0))

            if form_delete.submit_btn_conf_del.data:
                # Récupère les données afin d'afficher à nouveau
                # le formulaire "genres/genre_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
                data_films_attribue_genre_delete = session['data_films_attribue_genre_delete']
                print("data_films_attribue_genre_delete ", data_films_attribue_genre_delete)

                flash(f"Effacer le document de façon définitive de la BD !!!", "danger")
                # L'utilisateur vient de cliquer sur le bouton de confirmation pour effacer...
                # On affiche le bouton "Effacer genre" qui va irrémédiablement EFFACER le genre
                btn_submit_del = True

            if form_delete.submit_btn_del.data:
                valeur_delete_dictionnaire = {"value_id_genre": id_genre_delete}
                print("valeur_delete_dictionnaire ", valeur_delete_dictionnaire)

                str_sql_delete_films_genre = """DELETE FROM t_doc_have_cat WHERE fk_doc = %(value_id_genre)s"""
                str_sql_delete_idgenre = """DELETE FROM t_document WHERE id_document = %(value_id_genre)s"""
                # Manière brutale d'effacer d'abord la "fk_genre", même si elle n'existe pas dans la "t_genre_film"
                # Ensuite on peut effacer le genre vu qu'il n'est plus "lié" (INNODB) dans la "t_genre_film"
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(str_sql_delete_films_genre, valeur_delete_dictionnaire)
                    mconn_bd.mabd_execute(str_sql_delete_idgenre, valeur_delete_dictionnaire)

                flash(f"Document définitivement effacé !!", "success")
                print(f"Document définitivement effacé !!")

                # afficher les données
                #return redirect(url_for('documents_afficher', order_by="ASC", id_genre_sel=0))
                return redirect(url_for('films_genres_afficher', id_film_sel=0))

        if request.method == "GET":
            valeur_select_dictionnaire = {"value_id_genre": id_genre_delete}
            print(id_genre_delete, type(id_genre_delete))

            # Requête qui affiche tous les films qui ont le genre que l'utilisateur veut effacer
            str_sql_genres_films_delete = """SELECT id_doc_have_cat, docName, id_cat, catName FROM t_doc_have_cat 
                                            INNER JOIN t_document ON t_doc_have_cat.fk_doc = t_document.id_document
                                            INNER JOIN t_category ON t_doc_have_cat.fk_cat = t_category.id_cat
                                            WHERE fk_doc = %(value_id_genre)s"""

            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()

            mybd_curseur.execute(str_sql_genres_films_delete, valeur_select_dictionnaire)
            data_films_attribue_genre_delete = mybd_curseur.fetchall()
            print("data_films_attribue_genre_delete...", data_films_attribue_genre_delete)

            # Nécessaire pour mémoriser les données afin d'afficher à nouveau
            # le formulaire "genres/genre_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
            session['data_films_attribue_genre_delete'] = data_films_attribue_genre_delete

            # Opération sur la BD pour récupérer "id_genre" et "intitule_genre" de la "t_genre"
            str_sql_id_genre = "SELECT id_document, docName FROM t_document WHERE id_document = %(value_id_genre)s"

            mybd_curseur.execute(str_sql_id_genre, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()",
            # vu qu'il n'y a qu'un seul champ "nom genre" pour l'action DELETE
            data_nom_genre = mybd_curseur.fetchone()
            print("data_nom_genre ", data_nom_genre, " type ", type(data_nom_genre), " genre ",
                  data_nom_genre["docName"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "genre_delete_wtf.html"
            form_delete.nom_genre_delete_wtf.data = data_nom_genre["docName"]

            # Le bouton pour l'action "DELETE" dans le form. "genre_delete_wtf.html" est caché.
            btn_submit_del = False

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans genre_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans genre_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")

        flash(f"Erreur dans genre_delete_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")

        flash(f"__KeyError dans genre_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("documents/document_delete_wtf.html",
                           form_delete=form_delete,
                           btn_submit_del=btn_submit_del,
                           data_films_associes=data_films_attribue_genre_delete)
