"""
    Fichier : gestion_genres_wtf_forms.py
    Auteur : OM 2021.03.22
    Gestion des formulaires avec WTF
"""
from flask_wtf import FlaskForm
from wtforms import BooleanField
from wtforms import PasswordField
from wtforms import StringField
from wtforms import SubmitField
from wtforms import TextAreaField
from wtforms import SelectField
from wtforms import FileField
from flask_wtf.file import FileField, FileAllowed, FileRequired
from wtforms.validators import DataRequired
from wtforms.validators import Length
from wtforms.validators import Regexp
from wtforms.validators import Optional

# Flo > makes a field required if another field has a value in the form
# https://stackoverflow.com/questions/8463209/how-to-make-a-field-conditionally-optional-in-wtforms
# ne marche pas, à (re)garder...
"""
class RequiredIf(Required):
    # a validator which makes a field required if
    # another field is set and has a truthy value

    def __init__(self, other_field_name, *args, **kwargs):
        self.other_field_name = other_field_name
        super(RequiredIf, self).__init__(*args, **kwargs)

    def __call__(self, form, field):
        other_field = form._fields.get(self.other_field_name)
        if other_field is None:
            raise Exception('no field named "%s" in form' % self.other_field_name)
        if bool(other_field.data):
            super(RequiredIf, self).__call__(form, field)
"""
# Flo > Test d'une AUTRE Classe
# https://gist.github.com/devxoul/7638142
class RequiredIf(DataRequired):
    """Validator which makes a field required if another field is set and has a truthy value.

    Sources:
        - http://wtforms.simplecodes.com/docs/1.0.1/validators.html
        - http://stackoverflow.com/questions/8463209/how-to-make-a-field-conditionally-optional-in-wtforms
        - https://gist.github.com/devxoul/7638142#file-wtf_required_if-py
    """
    field_flags = ('requiredif',)

    def __init__(self, message=None, *args, **kwargs):
        super(RequiredIf).__init__()
        self.message = message
        self.conditions = kwargs

    # field is requiring that name field in the form is data value in the form
    def __call__(self, form, field):
        for name, data in self.conditions.items():
            other_field = form[name]
            if other_field is None:
                raise Exception('no field named "%s" in form' % name)
            if other_field.data == data and not field.data:
                DataRequired.__call__(self, form, field)
            Optional()(form, field)

class FormWTFAjouterDocuments(FlaskForm):
    """
        Dans le formulaire "genres_ajouter_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.

        Regex 1: Seulement chiffres, lettres et underscore
        ^[A-Z0-9 _]*$
        ^[a-zA-Z0-9 _-]*$
    """
    choix_document_wtf = SelectField('Type d\'ajout (switch)',
                                    choices=[('upload', 'Uploder un fichier'), ('chemin', 'Saisir un chemin')],
                                    validate_choice=False)


    nom_document_regexp = "^[a-zA-ZÀ-ÖØ-öø-ÿ0-9 -']*$"
    nom_document_wtf = StringField("Nom du document ", validators=[Length(min=1, max=50, message="Ne peut pas etre vide"),
                                                                   Regexp(nom_document_regexp,
                                                                          message="Pas de caractères spéciaux")
                                                                   ])
    #Flo > REQUIS SI Chemin est séléctionné
    url_document_wtf = StringField("Chemin du document ", [RequiredIf(choix_document_wtf='chemin', message="URI requise car CHEMIN est sélectionné")])

    type_document_wtf = SelectField('Extension du document',
                                      validators=[RequiredIf(choix_document_wtf='chemin', message="Sélectionner un type, cool comme moi !")],
                                      validate_choice=True
                                      )

    desc_document_wtf = TextAreaField('Petite description ', validators=[])

    #Flo > REQUIS SI Upload est séléctionné
    file_document_wtf = FileField('Téléverser document', validators=[
        RequiredIf(choix_document_wtf='upload', message="Fichier requis car UPLOAD est sélectionné"),
        FileAllowed(['pdf', 'docx', 'xlsx', 'pptx'], 'Documents PDF, docx, xlsx ou pptx seulement')
    ])

    submit = SubmitField("Enregistrer document")




class FormWTFUpdateDocument(FlaskForm):
    """
        Dans le formulaire "genre_update_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """

    nom_document_update_regexp = "^[a-zA-ZÀ-ÖØ-öø-ÿ0-9 -']*$"
    nom_document_update_wtf = StringField("Nom du document ", validators=[Length(min=1, max=50, message="Ne peut pas etre vide"),
                                                                   Regexp(nom_document_update_regexp,
                                                                          message="Pas de caractères spéciaux")
                                                                   ])

    url_document_update_wtf = StringField("Chemin du document ")

    #type_document_update_wtf = SelectField('Extension du document')

    desc_document_update_wtf = TextAreaField('Petite description ', validators=[])

    submit = SubmitField("Mettre à jour le document")


class FormWTFDeleteDocument(FlaskForm):
    """
        Dans le formulaire "genre_delete_wtf.html"

        nom_genre_delete_wtf : Champ qui reçoit la valeur du genre, lecture seule. (readonly=true)
        submit_btn_del : Bouton d'effacement "DEFINITIF".
        submit_btn_conf_del : Bouton de confirmation pour effacer un "genre".
        submit_btn_annuler : Bouton qui permet d'afficher la table "t_genre".
    """
    nom_genre_delete_wtf = StringField("Effacer ce document")
    submit_btn_del = SubmitField("Effacer document")
    submit_btn_conf_del = SubmitField("Etes-vous sur d'effacer ?")
    submit_btn_annuler = SubmitField("Annuler")
