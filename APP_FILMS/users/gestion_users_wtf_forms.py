"""
    Fichier : gestion_users_wtf_forms.py
    Auteur : OM 2021.03.22
    Gestion des formulaires avec WTF
"""
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms import SubmitField
from wtforms import TextAreaField
from wtforms import PasswordField
from wtforms import validators
from wtforms.validators import DataRequired
from wtforms.validators import Length
from wtforms.validators import Regexp
#from wtforms.validators import email
from wtforms.validators import equal_to


class FormWTFAjouterUsers(FlaskForm):
    """
        Dans le formulaire "users_ajouter_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    nom_users_regexp = "^[a-z]*$"
    nom_users_wtf = StringField("Nom ", validators=[DataRequired(message="Requis  "),
                            Regexp(nom_users_regexp,message="Que des lettres minuscules, pas d'espace"),
                            Length(min=4, max=35, message="Minimum 4 caractères")])

    # installer email_validator pour faire fonctionner la granularité des erreurs email !!! Florian G. / 08.05.21
    #email_users_wtf = StringField('Email ', validators=[DataRequired(message="Un email est requis  "), email(granular_message=True)])
    # sinon utiliser regex standard -->

    email_regex = '^(\w|\.|\_|\-)+[@](\w|\_|\-|\.)+[.]\w{2,3}$'
    email_users_wtf = StringField('Email ', validators=[DataRequired(message="Un email est requis  "),
                                                        Regexp(email_regex,message="Ne correspond pas à un email valide")])

    pass_users_wtf = PasswordField('Mot de passe', validators=[DataRequired(message="Requis"),
                            Length(min=4, max=35, message="Minimum 4 caractères"),
                            equal_to('pass2nd_users_wtf', message='Mot de passe n\'est pas identique')
                                                               ])
    pass2nd_users_wtf = PasswordField('Confirmer mot de passe')

    submit = SubmitField("Enregistrer utilisateur")


class FormWTFUpdateUsers(FlaskForm):
    """
        Dans le formulaire "users_update_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    nom_users_regexp = "^[a-z]*$"
    nom_users_update_wtf = StringField("Nom ", validators=[DataRequired(message="Requis  "),
                            Regexp(nom_users_regexp,message="Que des lettres minuscules, pas d'espace"),
                            Length(min=4, max=35, message="Minimum 4 caractères"),
                                                                   ])

    # installer email_validator pour faire fonctionner la granularité des erreurs email !!! Florian G. / 08.05.21
    #email_users_update_wtf = StringField('Email ', validators=[DataRequired(message="Un email est requis  "), email(granular_message=True)])

    email_regex = '^(\w|\.|\_|\-)+[@](\w|\_|\-|\.)+[.]\w{2,3}$'
    email_users_update_wtf = StringField('Email ', validators=[DataRequired(message="Un email est requis  "),
                                                               Regexp(email_regex,message="Ne correspond pas à un email valide")])

    submit = SubmitField("Update utilisateur")


class FormWTFDeleteUsers(FlaskForm):
    """
        Dans le formulaire "users_delete_wtf.html"

        nom_users_delete_wtf : Champ qui reçoit la valeur du users, lecture seule. (readonly=true)
        submit_btn_del : Bouton d'effacement "DEFINITIF".
        submit_btn_conf_del : Bouton de confirmation pour effacer un "users".
        submit_btn_annuler : Bouton qui permet d'afficher la table "t_users".
    """
    nom_users_delete_wtf = StringField("Effacer cet utilisateur")
    submit_btn_del = SubmitField("Effacer utilisateur")
    submit_btn_conf_del = SubmitField("Etes-vous sur d'effacer ?")
    submit_btn_annuler = SubmitField("Annuler")
