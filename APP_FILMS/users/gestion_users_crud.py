"""
    Fichier : gestion_users_crud.py
    Auteur : FG 2021.05.06
    Gestions des "routes" FLASK et des données pour les users.
"""
import sys

import pymysql
from flask import flash
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for

from werkzeug.security import generate_password_hash, check_password_hash

from APP_FILMS import obj_mon_application
from APP_FILMS.database.connect_db_context_manager import MaBaseDeDonnee
from APP_FILMS.erreurs.exceptions import *
from APP_FILMS.erreurs.msg_erreurs import *
from APP_FILMS.users.gestion_users_wtf_forms import FormWTFAjouterUsers
from APP_FILMS.users.gestion_users_wtf_forms import FormWTFDeleteUsers
from APP_FILMS.users.gestion_users_wtf_forms import FormWTFUpdateUsers

"""
    Auteur : OM 2021.03.16
    Définition d'une "route" /users_afficher
    
    Test : ex : http://127.0.0.1:5005/users_afficher
    
    Paramètres : order_by : ASC : Ascendant, DESC : Descendant
                id_users_sel = 0 >> tous les users.
                id_users_sel = "n" affiche le users dont l'id est "n"
"""


@obj_mon_application.route("/users_afficher/<string:order_by>/<int:id_users_sel>", methods=['GET', 'POST'])
def users_afficher(order_by, id_users_sel):
    if request.method == "GET":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion users ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur GestionUsers {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                if order_by == "ASC" and id_users_sel == 0:
                    strsql_users_afficher = """SELECT id_user, userName, userEmail, userPassword, lastLogin FROM t_user ORDER BY id_user ASC"""
                    mc_afficher.execute(strsql_users_afficher)
                elif order_by == "ASC":
                    # C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
                    # la commande MySql classique est "SELECT * FROM t_users"
                    # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
                    # donc, je précise les champs à afficher
                    # Constitution d'un dictionnaire pour associer l'id du users sélectionné avec un nom de variable
                    valeur_id_users_selected_dictionnaire = {"value_id_users_selected": id_users_sel}
                    strsql_users_afficher = """SELECT id_user, userName, userEmail, userPassword, lastLogin FROM t_user WHERE id_user = %(value_id_users_selected)s"""

                    mc_afficher.execute(strsql_users_afficher, valeur_id_users_selected_dictionnaire)
                else:
                    strsql_users_afficher = """SELECT id_user, userName, userEmail, userPassword, lastLogin FROM t_user ORDER BY id_user DESC"""

                    mc_afficher.execute(strsql_users_afficher)

                data_users = mc_afficher.fetchall()

                print("data_users ", data_users, " Type : ", type(data_users))

                # Différencier les messages si la table est vide.
                if not data_users and id_users_sel == 0:
                    flash("""La table "t_user" est vide. !!""", "warning")
                elif not data_users and id_users_sel > 0:
                    # Si l'utilisateur change l'id_users dans l'URL et que le users n'existe pas,
                    flash(f"L'utilisateur demandé n'existe pas !!", "warning")
                else:
                    # Dans tous les autres cas, c'est que la table "t_users" est vide.
                    # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                    flash(f"Données utilisateurs affichées !!", "success")

        except Exception as erreur:
            print(f"RGG Erreur générale. users_afficher")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)"
            # fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            flash(f"RGG Exception {erreur} users_afficher", "danger")
            raise Exception(f"RGG Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # Envoie la page "HTML" au serveur.
    return render_template("users/users_afficher.html", data=data_users)


"""
    Auteur : OM 2021.03.22
    Définition d'une "route" /users_ajouter
    
    Test : ex : http://127.0.0.1:5005/users_ajouter
    
    Paramètres : sans
    
    But : Ajouter un users pour un film
    
    Remarque :  Dans le champ "name_users_html" du formulaire "users/users_ajouter.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/users_ajouter", methods=['GET', 'POST'])
def users_ajouter_wtf():
    form = FormWTFAjouterUsers()
    if request.method == "POST":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion users ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur Gestionusers {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            if form.validate_on_submit():
                name_users_wtf = form.nom_users_wtf.data
                email_users_wtf = form.email_users_wtf.data
                pass_users_wtf = form.pass_users_wtf.data

                #name_users = name_users_wtf.lower()
                name_users = name_users_wtf
                email_users = email_users_wtf
                pass_users = generate_password_hash(pass_users_wtf)
                print(pass_users)
                #passhash_users = generate_password_hash()

                valeurs_insertion_dictionnaire = {"value_intitule_users": name_users, "value_email_users": email_users, "value_pass_users": pass_users}
                print("valeurs_insertion_dictionnaire ", valeurs_insertion_dictionnaire)

                strsql_insert_users = """INSERT INTO t_user (id_user,userName,userEmail,userPassword) VALUES (NULL,%(value_intitule_users)s,%(value_email_users)s,%(value_pass_users)s)"""
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(strsql_insert_users, valeurs_insertion_dictionnaire)

                flash(f"Données insérées !!", "success")
                print(f"Données insérées !!")

                # Pour afficher et constater l'insertion de la valeur, on affiche en ordre inverse. (DESC)
                return redirect(url_for('users_afficher', order_by='DESC', id_users_sel=0))

        # ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur_users_doublon:
            # Dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs/exceptions.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            code, msg = erreur_users_doublon.args

            flash(f"{error_codes.get(code, msg)} ", "warning")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur_gest_genr_crud:
            code, msg = erreur_gest_genr_crud.args

            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Erreur dans Gestion users CRUD : {sys.exc_info()[0]} "
                  f"{erreur_gest_genr_crud.args[0]} , "
                  f"{erreur_gest_genr_crud}", "danger")

    return render_template("users/users_ajouter_wtf.html", form=form)


"""
    Auteur : OM 2021.03.29
    Définition d'une "route" /users_update
    
    Test : ex cliquer sur le menu "users" puis cliquer sur le bouton "EDIT" d'un "users"
    
    Paramètres : sans
    
    But : Editer(update) un users qui a été sélectionné dans le formulaire "users_afficher.html"
    
    Remarque :  Dans le champ "nom_users_update_wtf" du formulaire "users/users_update_wtf.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/users_update", methods=['GET', 'POST'])
def users_update_wtf():

    # L'utilisateur vient de cliquer sur le bouton "EDIT". Récupère la valeur de "id_users"
    id_users_update = request.values['id_users_btn_edit_html']

    # Objet formulaire pour l'UPDATE
    form_update = FormWTFUpdateUsers()
    try:
        print(" on submit ", form_update.validate_on_submit())
        if form_update.validate_on_submit():
            # Récupèrer la valeur du champ depuis "users_update_wtf.html" après avoir cliqué sur "SUBMIT".
            # Puis la convertir en lettres minuscules.
            name_users_update = form_update.nom_users_update_wtf.data
            email_users_update = form_update.email_users_update_wtf.data




            valeur_update_dictionnaire = {"value_id_users": id_users_update, "value_name_users": name_users_update, "value_email_users": email_users_update}
            print("valeur_update_dictionnaire ", valeur_update_dictionnaire)

            str_sql_update_intituleusers = """UPDATE t_user SET userName = %(value_name_users)s, userEmail = %(value_email_users)s WHERE id_user = %(value_id_users)s"""
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(str_sql_update_intituleusers, valeur_update_dictionnaire)

            flash(f"Donnée mise à jour !!", "success")
            print(f"Donnée mise à jour !!")

            # afficher et constater que la donnée est mise à jour.
            # Affiche seulement la valeur modifiée, "ASC" et l'"id_users_update"
            return redirect(url_for('users_afficher', order_by="ASC", id_users_sel=id_users_update))
        elif request.method == "GET":
            # Opération sur la BD pour récupérer "id_users" et "intitule_users" de la "t_users"
            str_sql_id_users = "SELECT id_user, userName, userEmail FROM t_user WHERE id_user = %(value_id_users)s"
            valeur_select_dictionnaire = {"value_id_users": id_users_update}
            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()
            mybd_curseur.execute(str_sql_id_users, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()", vu qu'il n'y a qu'un seul champ "nom users" pour l'UPDATE
            data_nom_users = mybd_curseur.fetchone()
            print("data_nom_users ", data_nom_users, " type ", type(data_nom_users), " users ",
                  data_nom_users["userName"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "users_update_wtf.html"
            form_update.nom_users_update_wtf.data = data_nom_users["userName"]
            form_update.email_users_update_wtf.data = data_nom_users["userEmail"]

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans users_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans users_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")
        flash(f"Erreur dans users_update_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")
        flash(f"__KeyError dans users_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("users/users_update_wtf.html", form_update=form_update)


"""
    Auteur : OM 2021.04.08
    Définition d'une "route" /users_delete
    
    Test : ex. cliquer sur le menu "users" puis cliquer sur le bouton "DELETE" d'un "users"
    
    Paramètres : sans
    
    But : Effacer(delete) un users qui a été sélectionné dans le formulaire "users_afficher.html"
    
    Remarque :  Dans le champ "nom_users_delete_wtf" du formulaire "users/users_delete_wtf.html",
                le contrôle de la saisie est désactivée. On doit simplement cliquer sur "DELETE"
"""


@obj_mon_application.route("/users_delete", methods=['GET', 'POST'])
def users_delete_wtf():
    data_films_attribue_users_delete = None
    btn_submit_del = None
    # L'utilisateur vient de cliquer sur le bouton "DELETE". Récupère la valeur de "id_users"
    id_users_delete = request.values['id_users_btn_delete_html']

    # Objet formulaire pour effacer le users sélectionné.
    form_delete = FormWTFDeleteUsers()
    try:
        print(" on submit ", form_delete.validate_on_submit())
        if request.method == "POST" and form_delete.validate_on_submit():

            if form_delete.submit_btn_annuler.data:
                return redirect(url_for("users_afficher", order_by="ASC", id_users_sel=0))

            if form_delete.submit_btn_conf_del.data:
                # Récupère les données afin d'afficher à nouveau
                # le formulaire "users/users_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
                # data_films_attribue_users_delete = session['data_films_attribue_users_delete']
                # print("data_films_attribue_users_delete ", data_films_attribue_users_delete)

                flash(f"Effacer l'utilisateur de façon définitive de la BD !!!", "danger")
                # L'utilisateur vient de cliquer sur le bouton de confirmation pour effacer...
                # On affiche le bouton "Effacer users" qui va irrémédiablement EFFACER le users
                btn_submit_del = True

            if form_delete.submit_btn_del.data:
                valeur_delete_dictionnaire = {"value_id_users": id_users_delete}
                print("valeur_delete_dictionnaire ", valeur_delete_dictionnaire)

                # Flo > Correctif de dernière minute pour ne pas laisser la table t_user sans relation
                # Flo > Efface à l'arrache tous les documents de l'utilisateur puis l'utilisateur
                # str_sql_delete_doc_idusers = """DELETE FROM t_document,t_doc_have_cat USING t_document INNER JOIN t_doc_have_cat on (t_document.id_document = t_doc_have_cat.fk_doc) WHERE t_document.fk_user = %(value_id_users)s"""
                str_sql_delete_idusers = """DELETE FROM t_user WHERE id_user = %(value_id_users)s"""
                # Manière brutale d'effacer d'abord la "fk_users", même si elle n'existe pas dans la "t_users_film"
                # Ensuite on peut effacer le users vu qu'il n'est plus "lié" (INNODB) dans la "t_users_film"
                with MaBaseDeDonnee() as mconn_bd:
                    #mconn_bd.mabd_execute(str_sql_delete_doc_idusers, valeur_delete_dictionnaire)
                    mconn_bd.mabd_execute(str_sql_delete_idusers, valeur_delete_dictionnaire)

                flash(f"Utilisateur définitivement effacé !!", "success")
                print(f"Utilisateur définitivement effacé !!")

                # afficher les données
                return redirect(url_for('users_afficher', order_by="ASC", id_users_sel=0))

        if request.method == "GET":
            valeur_select_dictionnaire = {"value_id_users": id_users_delete}
            print(id_users_delete, type(id_users_delete))

            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()

            # Opération sur la BD pour récupérer "id_users" et "intitule_users" de la "t_users"
            str_sql_id_users = "SELECT id_user, userName FROM t_user WHERE id_user = %(value_id_users)s"

            mybd_curseur.execute(str_sql_id_users, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()",
            # vu qu'il n'y a qu'un seul champ "nom users" pour l'action DELETE
            data_nom_users = mybd_curseur.fetchone()
            print("data_nom_users ", data_nom_users, " type ", type(data_nom_users), " users ",
                  data_nom_users["userName"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "users_delete_wtf.html"
            form_delete.nom_users_delete_wtf.data = data_nom_users["userName"]

            # Le bouton pour l'action "DELETE" dans le form. "users_delete_wtf.html" est caché.
            btn_submit_del = False

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans users_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans users_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")

        flash(f"Erreur dans users_delete_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")

        flash(f"__KeyError dans users_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("users/users_delete_wtf.html",
                           form_delete=form_delete,
                           btn_submit_del=btn_submit_del,
                           data_films_associes=data_films_attribue_users_delete)
