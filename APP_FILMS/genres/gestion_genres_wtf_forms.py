"""
    Fichier : gestion_genres_wtf_forms.py
    Auteur : OM 2021.03.22
    Gestion des formulaires avec WTF
"""
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms import SubmitField
from wtforms import TextAreaField
from wtforms.validators import DataRequired
from wtforms.validators import Length
from wtforms.validators import Regexp


class FormWTFAjouterGenres(FlaskForm):
    """
        Dans le formulaire "genres_ajouter_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    # Flo > Tous les caractères, + accentués, chiffres, trait d union, espace, apostrophe
    nom_genre_regexp = "^[a-zA-ZÀ-ÖØ-öø-ÿ0-9 -']*$"
    nom_genre_wtf = StringField("Saisissez une catégorie ", validators=[Length(min=2, max=30, message="min 2 max 30"),
                                                                   Regexp(nom_genre_regexp,
                                                                          message="Pas de caractères spéciaux")
                                                                   ])
    desc_genre_wtf = TextAreaField('Description catégorie ', validators=[DataRequired(message="Une mini description est requise")])

    # Flo pense bête -----------------------------------------------
    # ^[0-9]*$ chiffres ou empty ->  ^[0-9]+$ chiffres et pas empty
    # --------------------------------------------------------------

    rang_genre_regexp = "^[0-9]+$"
    rang_genre_wtf = StringField("Ajouter un rang ( 0 à 99 )", validators=[Length(min=1, max=2, message="min 0 max 99"),
                                                                   Regexp(rang_genre_regexp,
                                                                          message="Seulement des chiffres et entiers max 99")
                                                                   ])

    submit = SubmitField("Enregistrer catégorie")


class FormWTFUpdateGenre(FlaskForm):
    """
        Dans le formulaire "genre_update_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    nom_genre_update_regexp = "^[a-zA-ZÀ-ÖØ-öø-ÿ0-9 -']*$"
    nom_genre_update_wtf = StringField("Saisissez une catégorie ", validators=[Length(min=2, max=30, message="min 2 max 30"),
                                                                          Regexp(nom_genre_update_regexp,
                                                                                 message="Pas de caractères spéciaux")
                                                                          ])

    desc_genre_update_wtf = TextAreaField('Description catégorie ', validators=[DataRequired(message="une mini description est requise")])

    rang_genre_update_regexp = "^[0-9]+$"
    rang_genre_update_wtf = StringField("Ajouter un rang ( 0 à 99 )", validators=[Length(min=1, max=2, message="min 0 max 99"),
                                                                   Regexp(rang_genre_update_regexp,
                                                                          message="Seulement des chiffres et entiers max 99")
                                                                   ])

    submit = SubmitField("Update catégorie")


class FormWTFDeleteGenre(FlaskForm):
    """
        Dans le formulaire "genre_delete_wtf.html"

        nom_genre_delete_wtf : Champ qui reçoit la valeur du genre, lecture seule. (readonly=true)
        submit_btn_del : Bouton d'effacement "DEFINITIF".
        submit_btn_conf_del : Bouton de confirmation pour effacer un "genre".
        submit_btn_annuler : Bouton qui permet d'afficher la table "t_genre".
    """
    nom_genre_delete_wtf = StringField("Effacer cette catégorie")
    submit_btn_del = SubmitField("Effacer catégorie")
    submit_btn_conf_del = SubmitField("Etes-vous sur d'effacer ?")
    submit_btn_annuler = SubmitField("Annuler")
