"""
    Fichier : routes_demos.py
    Auteur : Flo 2021.06.14
    Pour faire des tests divers et variés, avec la notion de "routes" avec FLASK
"""
# Flo > Il faut OS
import os
from flask import render_template
from flask import request
from flask import jsonify
from flask import flash
from flask import redirect
# Flo > Import de dépendances supplémentaires
from flask import session, url_for, abort
from flask import send_from_directory
# Flo > Import de trucs de signatures de fichiers
# constants
from APP_FILMS.pyfsig.file_signatures import signatures
# classes
from APP_FILMS.pyfsig.file_signatures import Signature, Matches, NoMatchException
# defs
from APP_FILMS.pyfsig.file_signatures import compare_sig, get_from_file, get_from_path
# Flo ---------------------------------------
from APP_FILMS import obj_mon_application
from APP_FILMS.erreurs.msg_erreurs import *
from APP_FILMS.erreurs.exceptions import *
from APP_FILMS.database.database_tools import Toolsbd

# Flo > Divers trucs pour la sécurité des données
from werkzeug.security import check_password_hash
from werkzeug.utils import secure_filename



@obj_mon_application.route('/index')
def index():
    return "Hello, en retour Mr. Maccaud !"

@obj_mon_application.route('/panel')
def panel():
    if 'loggedin' in session:
        # Flo - Si identifié retourne la page admin, avec identifiant
        # return redirect(url_for('panel', username=session ['username']))
        objet_connectbd = Toolsbd()
        connect_mabd = objet_connectbd.connect_database()
        curseur_mabd = connect_mabd.cursor()

        #Flo > Requette pour compter le nombre de fichier trié par extensions
        curseur_mabd.execute("SELECT fileType, fileRGB, Count(d.id_document) AS fileNumber FROM t_type t LEFT JOIN t_document d on d.fk_type = t.id_type GROUP BY d.fk_type ORDER BY fileNumber DESC")
        charts = curseur_mabd.fetchall()

        curseur_mabd.execute("SELECT Count(id_document) AS nombre FROM t_document")
        nbdoc = curseur_mabd.fetchall()
        print('Docs ', nbdoc)

        curseur_mabd.execute("SELECT Count(id_cat) AS nombre FROM t_category")
        nbcat = curseur_mabd.fetchall()
        print('Cats ', nbdoc)

        curseur_mabd.execute("SELECT Count(id_user) AS nombre FROM t_user")
        nbuser = curseur_mabd.fetchall()
        print('Users ', nbuser)

        curseur_mabd.execute("SELECT docName, docURI, fileType, fileRGB FROM t_document d INNER JOIN t_type t on d.fk_type = t.id_type ORDER BY id_document DESC LIMIT 5")
        lastdocs = curseur_mabd.fetchall()
        print('Last docs ', lastdocs)



        return render_template('home.html', charts=charts, nbdoc=nbdoc, nbcat=nbcat, nbuser=nbuser, lastdocs=lastdocs)
    return redirect(url_for('login'))

@obj_mon_application.route('/')
@obj_mon_application.route('/librairie')
def librairie():

    objet_connectbd = Toolsbd()
    connect_mabd = objet_connectbd.connect_database()
    curseur_mabd = connect_mabd.cursor()

    curseur_mabd.execute("SELECT * FROM t_category ORDER BY catRang ASC")
    content = curseur_mabd.fetchall()

    curseur_mabd.execute("SELECT id_document, docName, docURI, docDesc, docUpdate, catName, catRang, fileType FROM t_doc_have_cat RIGHT JOIN t_document ON t_document.id_document = t_doc_have_cat.fk_doc LEFT JOIN t_category ON t_category.id_cat = t_doc_have_cat.fk_cat LEFT JOIN t_type ON  t_type.id_type = t_document.fk_type ORDER BY catRang ASC, docName ASC ")
    docs = curseur_mabd.fetchall()

    return render_template('librairie.html', content=content, docs=docs)

@obj_mon_application.route('/login', methods=['GET', 'POST'])
def login():

    if 'loggedin' in session:
        # Flo - Si identifié retourne la page admin, avec identifiant
        return redirect(url_for('panel', username=session ['username']))

    objet_connectbd = Toolsbd()
    connect_mabd = objet_connectbd.connect_database()
    curseur_mabd = connect_mabd.cursor()

    # Check si la requete est POST et qu'elle contient le nom d'utilisateur et mot de passe
    if request.method == 'POST' and 'username' in request.form and 'password' in request.form:
        username = request.form['username']
        password = request.form['password']
        print(password)

        # Cherche si il trouve le nom d'utilisateur en BDD
        curseur_mabd.execute('SELECT * FROM t_user WHERE userName = %s', (username,))
        # Retourne le résultat
        account = curseur_mabd.fetchone()

        if account:
            password_rs = account['userPassword']
            print(password_rs)
            # Check le Hash de la BDD
            if check_password_hash(password_rs, password):
                # Créer les sessions
                session['loggedin'] = True
                session['id'] = account['id_user']
                session['username'] = account['userName']
                # Redirige sur le panel d'administration
                return redirect(url_for('panel'))
            else:
                flash('Nom d\'utilisateur ou mot de passe incorrect')
        else:
            flash('Nom d\'utilisateur ou mot de passe incorrect')

    return render_template('login.html')

@obj_mon_application.route('/logout')
def logout():
    # Flo -> Enlève toutes les données de SESSION
   session.pop('loggedin', None)
   session.pop('id', None)
   session.pop('username', None)
   # Redirige sur la page Login après l'effacement
   return redirect(url_for('login'))

@obj_mon_application.route('/dragorder')
def dragorder():
    objet_connectbd = Toolsbd()
    connect_mabd = objet_connectbd.connect_database()
    curseur_mabd = connect_mabd.cursor()

    curseur_mabd.execute("SELECT * FROM t_category ORDER BY catRang ASC")
    dragdrop = curseur_mabd.fetchall()

    return render_template('dragdrop.html', dragdrop=dragdrop)

@obj_mon_application.route("/updateList", methods=['GET', 'POST'])
def updateList():
    objet_connectbd = Toolsbd()
    connect_mabd = objet_connectbd.connect_database()
    curseur_mabd = connect_mabd.cursor()
    print('ici ca fonctionne')
    if request.method == "POST":
        print('mais la pas')
        number_of_rows= curseur_mabd.execute("SELECT * FROM t_category")
        #print(number_of_rows)
        getorder = request.form['order']
        print(getorder)
        order = getorder.split(",", number_of_rows)
        count=0
        for value in order:
            count +=1
            print(count)
            curseur_mabd.execute("UPDATE t_category SET catRang = %s WHERE id_cat = %s ", [count, value])
            curseur_mabd.connection.commit()
        curseur_mabd.close()
    return jsonify('Mise à jour avec succès :)')

@obj_mon_application.route('/erreurs')
def erreurs():
    return render_template('erreurs.html')

@obj_mon_application.route('/hack')
def hack():
    return render_template('hack.html')

@obj_mon_application.route('/upload')
def upload():
    return render_template('upload.html')


@obj_mon_application.route('/upload', methods=['POST'])
def upload_files():
    uploaded_file = request.files['file']
    #filename = uploaded_file.filename
    #Flo une méthode de werkzeug pour sécuriser un peu plus
    filename = secure_filename(uploaded_file.filename)
    #print(uploaded_file.read())
    print(filename)
    fullpath = os.path.abspath(filename)
    print(fullpath)
    #with open(uploaded_file, "rb") as f:
    #    herp = get_from_file(f)
    #herp = get_from_path(uploaded_file)
    #herp = os.path.join(filename)
    #print(herp)
    if filename != '':
        # Flo > .PDF retourne .pdf
        file_ext_check = os.path.splitext(filename)[1].lower()
        #retourne pdf
        file_ext = os.path.splitext(filename)[1][1:].strip().lower()
        print(file_ext)

        #Flo > Check si l'extension fait partie des extensions autorisée dans __init__
        if file_ext_check not in obj_mon_application.config['UPLOAD_EXTENSIONS']:
            return "Document invalide", 400
        uploaded_file.save(os.path.join('APP_FILMS',obj_mon_application.config['UPLOAD_PATH'], filename))
        print(os.path.join('APP_FILMS',obj_mon_application.config['UPLOAD_PATH'], filename))

        #Flo > Check en BDD pour récuperer l'ID Type par rapport à l'extension de fichier
        objet_connectbd = Toolsbd()
        connect_mabd = objet_connectbd.connect_database()
        curseur_mabd = connect_mabd.cursor()

        curseur_mabd.execute('SELECT * FROM t_type WHERE fileType = %s', (file_ext,))
        checktype = curseur_mabd.fetchone()
        checktype = checktype['fileSignature']
        print('Type trouvé BDD : ',checktype)
        #herp = get_from_path(os.path.join('APP_FILMS',obj_mon_application.config['UPLOAD_PATH'], filename))
        #print(herp)
        fullpath = os.path.abspath(os.path.join('APP_FILMS',obj_mon_application.config['UPLOAD_PATH'], filename))
        print(fullpath)
        #with open(fullpath, "rb") as f:
        #    derp = get_from_file(f)
        typedefichier = get_from_path(fullpath)
        print(typedefichier)
        for types in typedefichier:
             if file_ext == types['file_extension']:
                 print('TYPE TROUVE! ', types['file_extension'])
                 print('Message de FLO : envoyé avec succès')
                 return redirect(url_for('upload'))
                 break
        return render_template('hack.html', content=typedefichier, check=checktype, fileext=file_ext)
        flash('Document envoyé avec succes')
    return redirect(url_for('upload'))

@obj_mon_application.route('/uploads/<filename>')
def download_file(filename):
    print('Cherche fichier')
    print(filename)
    print(obj_mon_application.config['UPLOAD_PATH'])
    # Flo > As attachement TRUE télécharge le fichier
    #return send_from_directory(obj_mon_application.config['UPLOAD_PATH'], filename, as_attachment=True)
    return send_from_directory(obj_mon_application.config['UPLOAD_PATH'], filename)

@obj_mon_application.route('/database/<filename>')
def database_file(filename):
    print('Cherche fichier : ', filename)
    return send_from_directory('database', filename)

@obj_mon_application.route('/essai')
def route_hommage_a_u_x_V_ictim_es_du_monstre_du_mod_1_0_4():
    return render_template("essai/template_pour_route_essai.html")

@obj_mon_application.route('/taillepersonne')
def personnes_taille_dict():
    # DEBUG bon marché : Pour afficher dans la console les valeurs des erreurs "customisées"
    # dans le fichier "erreurs/exceptions.py" et le type de ces valeurs.
    print("msg_erreurs ", msg_erreurs, "type msg_erreurs ", type(msg_erreurs))

    # Affiche les valeurs et les clés
    print(msg_erreurs.items())
    # Affiche les clés
    print(msg_erreurs.keys())
    # Afficher les valeurs
    print(msg_erreurs.values())

    # Affiche la valeur "message" du dictionnaire d'erreur "DATABASE/msg_erreurs.py"
    print("val dans le dict ", msg_erreurs['ErreurDictionnaire'])

    # Défini un petit dictionnaire
    taille_personnes_dict = {"OM": 194, "Gégé": 175, "Hugo": 163}
    #
    # OM 2020.04.09 Pour vos essais, il suffit d'enlever le # pour voir comment fonctionne pratiquement
    # le traitement de l'erreur.
    # Si la chaîne de caractère ne se trouve pas dans le dictionnaire ci-dessus il va y avoir une erreur "KeyError"
    # On la capture et on renvoie un texte "personnel" (custom error handler) à l'utilisateur.
    nom_personne = "OM"
    # nom_personne = "Gégé"
    # nom_personne = "Hugo"

    #nom_personne = "Pignon"

    try:
        # Tout se passe normalement
        print(f'{nom_personne} mesure {taille_personnes_dict[nom_personne]} [cm]')
    except KeyError:
        # Une personne ne se trouve pas dans le dictionnaire
        # DEBUG bon marché : Pour afficher un message d'erreur dans la console
        print(f"{nom_personne} n'existe pas !")

        # Pour vos essais... constater les différentes actions avec "raise"
        # pour les 2 ci-dessous, il renvoie une page "keyerror.html"
        # grâce à "@obj_mon_application.errorhandler(KeyError)" défini dans le fichier "run_mon_app.py"
        # raise KeyError
        # raise erreur

        # Il renvoie simplement la valeur du contenu de "erreur"
        # grâce à la classe "MonErreur(Exception)" dans le fichier "exceptions.py"
        # raise MonErreur(erreur)

        # Il renvoie un texte avec la valeur de "nom_personne" et c'est dans le fichier
        # grâce à la classe "MonErreur(Exception)" dans le fichier "exceptions.py"
        # raise MonErreur(f"il y a une erreur ! La personne {nom_personne} n'existe pas dans le dictionnaire")

        # Celle-ci est assez complète... mais il y a toujours mieux
        # Il renvoie un texte avec la valeur de "nom_personne" ainsi qu'un message personnalisé
        # grâce à la classe "MonErreur(Exception)" dans le fichier "exceptions.py"
        raise MonErreur(f"{msg_erreurs['ErreurDictionnaire']['message']} "
                        f"Le nom : {nom_personne} n'est pas une valeur contenue dans le dictionnaire, "
                        f"pour comprendre, il faut modifier la valeur à la ligne 66 du fichier 'routes_demos.py'")

    return render_template("zzz_essais_om_104/exception_raise_custom_om_104.html")
