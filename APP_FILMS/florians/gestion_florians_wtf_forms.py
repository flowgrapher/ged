"""
    Fichier : gestion_florians_wtf_forms.py
    Auteur : OM 2021.03.22
    Gestion des formulaires avec WTF
"""
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms import SubmitField
from wtforms import TextAreaField
from wtforms.validators import DataRequired
from wtforms.validators import Length
from wtforms.validators import Regexp


class FormWTFAjouterFlorians(FlaskForm):
    """
        Dans le formulaire "florians_ajouter_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    nom_florians_regexp = "^[a-zA-ZÀ-ÖØ-öø-ÿ0-9 -']*$"
    nom_florians_wtf = StringField("Saisissez une catégorie ", validators=[Length(min=2, max=30, message="min 2 max 30"),
                                                                   Regexp(nom_florians_regexp,
                                                                          message="Pas de caractères spéciaux")
                                                                   ])
    desc_florians_wtf = TextAreaField('Description catégorie ', validators=[DataRequired(message="Une mini description est requise")])

    # Flo pense bête -----------------------------------------------
    # ^[0-9]*$ chiffres ou empty ->  ^[0-9]+$ chiffres et pas empty
    # --------------------------------------------------------------

    rang_florians_regexp = "^[0-9]+$"
    rang_florians_wtf = StringField("Ajouter un rang ( 0 à 99 )", validators=[Length(min=1, max=2, message="min 0 max 99"),
                                                                   Regexp(rang_florians_regexp,
                                                                          message="Seulement des chiffres et entiers max 99")
                                                                   ])

    submit = SubmitField("Enregistrer catégorie")


class FormWTFUpdateFlorians(FlaskForm):
    """
        Dans le formulaire "florians_update_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    nom_florians_update_regexp = "^[a-zA-ZÀ-ÖØ-öø-ÿ0-9 -']*$"
    nom_florians_update_wtf = StringField("Saisissez une catégorie ", validators=[Length(min=2, max=30, message="min 2 max 30"),
                                                                          Regexp(nom_florians_update_regexp,
                                                                                 message="Pas de caractères spéciaux")
                                                                          ])

    desc_florians_update_wtf = TextAreaField('Description catégorie ', validators=[DataRequired(message="une mini description est requise")])

    rang_florians_update_regexp = "^[0-9]+$"
    rang_florians_update_wtf = StringField("Ajouter un rang ( 0 à 99 )", validators=[Length(min=1, max=2, message="min 0 max 99"),
                                                                   Regexp(rang_florians_update_regexp,
                                                                          message="Seulement des chiffres et entiers max 99")
                                                                   ])

    submit = SubmitField("Update catégorie")


class FormWTFDeleteFlorians(FlaskForm):
    """
        Dans le formulaire "florians_delete_wtf.html"

        nom_florians_delete_wtf : Champ qui reçoit la valeur du florians, lecture seule. (readonly=true)
        submit_btn_del : Bouton d'effacement "DEFINITIF".
        submit_btn_conf_del : Bouton de confirmation pour effacer un "florians".
        submit_btn_annuler : Bouton qui permet d'afficher la table "t_florians".
    """
    nom_florians_delete_wtf = StringField("Effacer cette catégorie")
    submit_btn_del = SubmitField("Effacer catégorie")
    submit_btn_conf_del = SubmitField("Etes-vous sur d'effacer ?")
    submit_btn_annuler = SubmitField("Annuler")
