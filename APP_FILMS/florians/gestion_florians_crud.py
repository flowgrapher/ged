"""
    Fichier : gestion_florians_crud.py
    Auteur : FG 2021.05.06
    Gestions des "routes" FLASK et des données pour les florians.
"""
import sys

import pymysql
from flask import flash
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for

from APP_FILMS import obj_mon_application
from APP_FILMS.database.connect_db_context_manager import MaBaseDeDonnee
from APP_FILMS.erreurs.exceptions import *
from APP_FILMS.erreurs.msg_erreurs import *
from APP_FILMS.florians.gestion_florians_wtf_forms import FormWTFAjouterFlorians
from APP_FILMS.florians.gestion_florians_wtf_forms import FormWTFDeleteFlorians
from APP_FILMS.florians.gestion_florians_wtf_forms import FormWTFUpdateFlorians

"""
    Auteur : OM 2021.03.16
    Définition d'une "route" /florians_afficher
    
    Test : ex : http://127.0.0.1:5005/florians_afficher
    
    Paramètres : order_by : ASC : Ascendant, DESC : Descendant
                id_florians_sel = 0 >> tous les florians.
                id_florians_sel = "n" affiche le florians dont l'id est "n"
"""


@obj_mon_application.route("/florians_afficher/<string:order_by>/<int:id_florians_sel>", methods=['GET', 'POST'])
def florians_afficher(order_by, id_florians_sel):
    if request.method == "GET":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion florians ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur GestionFlorians {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                if order_by == "ASC" and id_florians_sel == 0:
                    strsql_florians_afficher = """SELECT id_cat, catName, catDesc, catRang FROM t_category ORDER BY id_cat ASC"""
                    mc_afficher.execute(strsql_florians_afficher)
                elif order_by == "ASC":
                    # C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
                    # la commande MySql classique est "SELECT * FROM t_florians"
                    # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
                    # donc, je précise les champs à afficher
                    # Constitution d'un dictionnaire pour associer l'id du florians sélectionné avec un nom de variable
                    valeur_id_florians_selected_dictionnaire = {"value_id_florians_selected": id_florians_sel}
                    strsql_florians_afficher = """SELECT id_cat, catName, catDesc, catRang FROM t_category WHERE id_cat = %(value_id_florians_selected)s"""

                    mc_afficher.execute(strsql_florians_afficher, valeur_id_florians_selected_dictionnaire)
                else:
                    strsql_florians_afficher = """SELECT id_cat, catName, catDesc, catRang FROM t_category ORDER BY id_cat DESC"""

                    mc_afficher.execute(strsql_florians_afficher)

                data_florians = mc_afficher.fetchall()

                print("data_florians ", data_florians, " Type : ", type(data_florians))

                # Différencier les messages si la table est vide.
                if not data_florians and id_florians_sel == 0:
                    flash("""La table "t_category" est vide. !!""", "warning")
                elif not data_florians and id_florians_sel > 0:
                    # Si l'utilisateur change l'id_florians dans l'URL et que le florians n'existe pas,
                    flash(f"La catégorie demandée n'existe pas !!", "warning")
                else:
                    # Dans tous les autres cas, c'est que la table "t_florians" est vide.
                    # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                    flash(f"Données catégories affichées !!", "success")

        except Exception as erreur:
            print(f"RGG Erreur générale. florians_afficher")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)"
            # fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            flash(f"RGG Exception {erreur} florians_afficher", "danger")
            raise Exception(f"RGG Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # Envoie la page "HTML" au serveur.
    return render_template("florians/florians_afficher.html", data=data_florians)


"""
    Auteur : OM 2021.03.22
    Définition d'une "route" /florians_ajouter
    
    Test : ex : http://127.0.0.1:5005/florians_ajouter
    
    Paramètres : sans
    
    But : Ajouter un florians pour un film
    
    Remarque :  Dans le champ "name_florians_html" du formulaire "florians/florians_ajouter.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/florians_ajouter", methods=['GET', 'POST'])
def florians_ajouter_wtf():
    form = FormWTFAjouterFlorians()
    if request.method == "POST":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion florians ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur Gestionflorians {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            if form.validate_on_submit():
                name_florians_wtf = form.nom_florians_wtf.data
                desc_florians_wtf = form.desc_florians_wtf.data
                rang_florians_wtf = form.rang_florians_wtf.data

                #name_florians = name_florians_wtf.lower()
                name_florians = name_florians_wtf
                desc_florians = desc_florians_wtf
                rang_florians = rang_florians_wtf

                valeurs_insertion_dictionnaire = {"value_intitule_florians": name_florians, "value_desc_florians": desc_florians, "value_rang_florians": rang_florians}
                print("valeurs_insertion_dictionnaire ", valeurs_insertion_dictionnaire)

                strsql_insert_florians = """INSERT INTO t_category (id_cat,catName,catDesc,catRang) VALUES (NULL,%(value_intitule_florians)s,%(value_desc_florians)s,%(value_rang_florians)s)"""
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(strsql_insert_florians, valeurs_insertion_dictionnaire)

                flash(f"Données insérées !!", "success")
                print(f"Données insérées !!")

                # Pour afficher et constater l'insertion de la valeur, on affiche en ordre inverse. (DESC)
                return redirect(url_for('florians_afficher', order_by='DESC', id_florians_sel=0))

        # ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur_florians_doublon:
            # Dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs/exceptions.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            code, msg = erreur_florians_doublon.args

            flash(f"{error_codes.get(code, msg)} ", "warning")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur_gest_genr_crud:
            code, msg = erreur_gest_genr_crud.args

            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Erreur dans Gestion florians CRUD : {sys.exc_info()[0]} "
                  f"{erreur_gest_genr_crud.args[0]} , "
                  f"{erreur_gest_genr_crud}", "danger")

    return render_template("florians/florians_ajouter_wtf.html", form=form)


"""
    Auteur : OM 2021.03.29
    Définition d'une "route" /florians_update
    
    Test : ex cliquer sur le menu "florians" puis cliquer sur le bouton "EDIT" d'un "florians"
    
    Paramètres : sans
    
    But : Editer(update) un florians qui a été sélectionné dans le formulaire "florians_afficher.html"
    
    Remarque :  Dans le champ "nom_florians_update_wtf" du formulaire "florians/florians_update_wtf.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/florians_update", methods=['GET', 'POST'])
def florians_update_wtf():

    # L'utilisateur vient de cliquer sur le bouton "EDIT". Récupère la valeur de "id_florians"
    id_florians_update = request.values['id_florians_btn_edit_html']

    # Objet formulaire pour l'UPDATE
    form_update = FormWTFUpdateFlorians()
    try:
        print(" on submit ", form_update.validate_on_submit())
        if form_update.validate_on_submit():
            # Récupèrer la valeur du champ depuis "florians_update_wtf.html" après avoir cliqué sur "SUBMIT".
            # Puis la convertir en lettres minuscules.
            name_florians_update = form_update.nom_florians_update_wtf.data
            desc_florians_update = form_update.desc_florians_update_wtf.data
            rang_florians_update = form_update.rang_florians_update_wtf.data
            # name_florians_update = name_florians_update.lower()

            valeur_update_dictionnaire = {"value_id_florians": id_florians_update, "value_name_florians": name_florians_update, "value_desc_florians": desc_florians_update, "value_rang_florians": rang_florians_update}
            print("valeur_update_dictionnaire ", valeur_update_dictionnaire)

            str_sql_update_intituleflorians = """UPDATE t_category SET catName = %(value_name_florians)s, catDesc = %(value_desc_florians)s, catRang = %(value_rang_florians)s WHERE id_cat = %(value_id_florians)s"""
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(str_sql_update_intituleflorians, valeur_update_dictionnaire)

            flash(f"Donnée mise à jour !!", "success")
            print(f"Donnée mise à jour !!")

            # afficher et constater que la donnée est mise à jour.
            # Affiche seulement la valeur modifiée, "ASC" et l'"id_florians_update"
            return redirect(url_for('florians_afficher', order_by="ASC", id_florians_sel=id_florians_update))
        elif request.method == "GET":
            # Opération sur la BD pour récupérer "id_florians" et "intitule_florians" de la "t_florians"
            str_sql_id_florians = "SELECT id_cat, catName, catDesc, catRang FROM t_category WHERE id_cat = %(value_id_florians)s"
            valeur_select_dictionnaire = {"value_id_florians": id_florians_update}
            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()
            mybd_curseur.execute(str_sql_id_florians, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()", vu qu'il n'y a qu'un seul champ "nom florians" pour l'UPDATE
            data_nom_florians = mybd_curseur.fetchone()
            print("data_nom_florians ", data_nom_florians, " type ", type(data_nom_florians), " florians ",
                  data_nom_florians["catName"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "florians_update_wtf.html"
            form_update.nom_florians_update_wtf.data = data_nom_florians["catName"]
            form_update.desc_florians_update_wtf.data = data_nom_florians["catDesc"]
            form_update.rang_florians_update_wtf.data = data_nom_florians["catRang"]

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans florians_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans florians_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")
        flash(f"Erreur dans florians_update_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")
        flash(f"__KeyError dans florians_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("florians/florians_update_wtf.html", form_update=form_update)


"""
    Auteur : OM 2021.04.08
    Définition d'une "route" /florians_delete
    
    Test : ex. cliquer sur le menu "florians" puis cliquer sur le bouton "DELETE" d'un "florians"
    
    Paramètres : sans
    
    But : Effacer(delete) un florians qui a été sélectionné dans le formulaire "florians_afficher.html"
    
    Remarque :  Dans le champ "nom_florians_delete_wtf" du formulaire "florians/florians_delete_wtf.html",
                le contrôle de la saisie est désactivée. On doit simplement cliquer sur "DELETE"
"""


@obj_mon_application.route("/florians_delete", methods=['GET', 'POST'])
def florians_delete_wtf():
    data_films_attribue_florians_delete = None
    btn_submit_del = None
    # L'utilisateur vient de cliquer sur le bouton "DELETE". Récupère la valeur de "id_florians"
    id_florians_delete = request.values['id_florians_btn_delete_html']

    # Objet formulaire pour effacer le florians sélectionné.
    form_delete = FormWTFDeleteFlorians()
    try:
        print(" on submit ", form_delete.validate_on_submit())
        if request.method == "POST" and form_delete.validate_on_submit():

            if form_delete.submit_btn_annuler.data:
                return redirect(url_for("florians_afficher", order_by="ASC", id_florians_sel=0))

            if form_delete.submit_btn_conf_del.data:
                # Récupère les données afin d'afficher à nouveau
                # le formulaire "florians/florians_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
                data_films_attribue_florians_delete = session['data_films_attribue_florians_delete']
                print("data_films_attribue_florians_delete ", data_films_attribue_florians_delete)

                flash(f"Effacer la catégorie de façon définitive de la BD !!!", "danger")
                # L'utilisateur vient de cliquer sur le bouton de confirmation pour effacer...
                # On affiche le bouton "Effacer florians" qui va irrémédiablement EFFACER le florians
                btn_submit_del = True

            if form_delete.submit_btn_del.data:
                valeur_delete_dictionnaire = {"value_id_florians": id_florians_delete}
                print("valeur_delete_dictionnaire ", valeur_delete_dictionnaire)

                str_sql_delete_films_florians = """DELETE FROM t_doc_have_cat WHERE fk_cat = %(value_id_florians)s"""
                str_sql_delete_idflorians = """DELETE FROM t_category WHERE id_cat = %(value_id_florians)s"""
                # Manière brutale d'effacer d'abord la "fk_florians", même si elle n'existe pas dans la "t_florians_film"
                # Ensuite on peut effacer le florians vu qu'il n'est plus "lié" (INNODB) dans la "t_florians_film"
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(str_sql_delete_films_florians, valeur_delete_dictionnaire)
                    mconn_bd.mabd_execute(str_sql_delete_idflorians, valeur_delete_dictionnaire)

                flash(f"Catégorie définitivement effacé !!", "success")
                print(f"Catégorie définitivement effacé !!")

                # afficher les données
                return redirect(url_for('florians_afficher', order_by="ASC", id_florians_sel=0))

        if request.method == "GET":
            valeur_select_dictionnaire = {"value_id_florians": id_florians_delete}
            print(id_florians_delete, type(id_florians_delete))

            # Requête qui affiche tous les documents qui ont la catégorie que l'utilisateur veut effacer
            str_sql_florians_films_delete = """SELECT id_doc_have_cat, docName, id_cat, catName FROM t_doc_have_cat 
                                            INNER JOIN t_document ON t_doc_have_cat.fk_doc = t_document.id_document
                                            INNER JOIN t_category ON t_doc_have_cat.fk_cat = t_category.id_cat
                                            WHERE fk_cat = %(value_id_florians)s"""

            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()

            mybd_curseur.execute(str_sql_florians_films_delete, valeur_select_dictionnaire)
            data_films_attribue_florians_delete = mybd_curseur.fetchall()
            print("data_films_attribue_florians_delete...", data_films_attribue_florians_delete)

            # Nécessaire pour mémoriser les données afin d'afficher à nouveau
            # le formulaire "florians/florians_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
            session['data_films_attribue_florians_delete'] = data_films_attribue_florians_delete

            # Opération sur la BD pour récupérer "id_florians" et "intitule_florians" de la "t_florians"
            str_sql_id_florians = "SELECT id_cat, catName FROM t_category WHERE id_cat = %(value_id_florians)s"

            mybd_curseur.execute(str_sql_id_florians, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()",
            # vu qu'il n'y a qu'un seul champ "nom florians" pour l'action DELETE
            data_nom_florians = mybd_curseur.fetchone()
            print("data_nom_florians ", data_nom_florians, " type ", type(data_nom_florians), " florians ",
                  data_nom_florians["catName"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "florians_delete_wtf.html"
            form_delete.nom_florians_delete_wtf.data = data_nom_florians["catName"]

            # Le bouton pour l'action "DELETE" dans le form. "florians_delete_wtf.html" est caché.
            btn_submit_del = False

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans florians_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans florians_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")

        flash(f"Erreur dans florians_delete_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")

        flash(f"__KeyError dans florians_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("florians/florians_delete_wtf.html",
                           form_delete=form_delete,
                           btn_submit_del=btn_submit_del,
                           data_films_associes=data_films_attribue_florians_delete)
