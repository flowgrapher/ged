-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Ven 11 Juin 2021 à 05:59
-- Version du serveur :  5.7.11
-- Version de PHP :  5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `gay_florian_info1c_ged_104_2021`
--

DROP DATABASE IF EXISTS gay_florian_info1c_ged_104_2021;

-- Création d'un nouvelle base de donnée

CREATE DATABASE IF NOT EXISTS gay_florian_info1c_ged_104_2021;

-- Utilisation de cette base de donnée

USE gay_florian_info1c_ged_104_2021;

-- --------------------------------------------------------

--
-- Structure de la table `t_category`
--

CREATE TABLE `t_category` (
  `id_cat` int(11) NOT NULL,
  `catName` varchar(30) NOT NULL,
  `catDesc` tinytext,
  `catRang` tinyint(3) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_category`
--

INSERT INTO `t_category` (`id_cat`, `catName`, `catDesc`, `catRang`) VALUES
(1, 'Manuels d\'utilisation', 'Les manuels sur comment faire les choses bien !!!', 3),
(3, 'Comptabilité', 'Comptabilité des chiffres de l\'entreprise', 8),
(4, 'Documents Secrets', 'Top secrets 007', 2),
(5, 'Info1Champions', 'Souvenirs info1C alias champions 2020/21', 1),
(7, 'Florian Gay 104', 'Documents à rendre pour le module 104', 4),
(8, 'Cours de l\'EPSIC', 'Drag & Drop des catégories', 5),
(10, 'Acc\'ents éàïö', 'Test accents et rangs', 7),
(11, 'Administratif', 'Document relatifs à l\'administration de l\'entreprise', 6);

-- --------------------------------------------------------

--
-- Structure de la table `t_document`
--

CREATE TABLE `t_document` (
  `id_document` int(11) NOT NULL,
  `docName` varchar(50) NOT NULL,
  `docURI` varchar(255) NOT NULL,
  `docDesc` text NOT NULL,
  `docCreation` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `docUpdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `docVersion` float NOT NULL DEFAULT '1',
  `fk_type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_document`
--

INSERT INTO `t_document` (`id_document`, `docName`, `docURI`, `docDesc`, `docCreation`, `docUpdate`, `docVersion`, `fk_type`) VALUES
(1, 'Mon premier document', 'D:\\Cours EPSIC\\Module 104\\ged\\', 'Présentation de mon tout 1er document', '2021-04-27 10:23:20', '2021-04-27 10:23:20', 1.1, 2),
(2, 'Présentation quelconque', '//dans_le_nuage/presentation/divers/claudette.pdf', 'Document sans grande importance', '2021-04-27 10:25:27', '2021-04-27 10:42:17', 1, 4),
(3, 'Règlement d\'utilisation', 'foo://username:password@example.com:8042/over/there/index.dtb?type=animal;name=ferret#nose', 'Avec des règles bizarres', '2021-04-27 10:29:07', '2021-04-27 10:29:07', 3.2, 2),
(7, 'Chemin de document', 'SRV://monpremierchemin\\/test', 'Mon premier chemin de document de type URI', '2021-05-13 11:55:51', '2021-05-13 11:55:51', 1, 1),
(8, 'Classes Adressage IPv4', 'uploads\\Classes_Adressage_IP_V4.pdf', 'Document sur les classes d\'adressage IPV4 vu au module 117', '2021-05-13 12:01:30', '2021-05-13 12:01:30', 1, 1),
(10, 'Carte débit', 'SRV://monpremierchemin\\/test', 'Ficher carte débit', '2021-05-21 11:32:36', '2021-05-21 11:32:36', 1, 1),
(14, 'Type de mise à jour', 'typedechemin.doc', 'Document Word', '2021-05-21 16:17:42', '2021-05-21 16:17:42', 1, 2),
(15, 'Unige document', 'uploads\\unige_40875_attachment01.pdf', 'ENcore un test', '2021-05-22 10:27:08', '2021-05-22 10:27:08', 1, 1),
(16, 'Tableau de données ', 'uploads\\document_excel.xlsx', 'Fichier Excel', '2021-05-22 12:58:27', '2021-05-22 12:58:27', 1, 3),
(17, 'Présentation PowerPoint du panier', 'uploads\\document_powerpoint.pptx', 'Une présentation PowerPoint ', '2021-05-22 13:07:37', '2021-05-22 13:07:37', 1, 4),
(18, 'Test du chemin V6', '//monreseau/nas/prive/doc', 'Test de chemin après avoir fix upload', '2021-05-22 13:58:05', '2021-05-22 13:58:05', 1, 2),
(19, 'STRAHM fait des vidéos', 'https://www.youtube.com/watch?v=jWIF9n4GXv8', 'Réalisation pour l\'ECG avec M. Gay et M. Rossi', '2021-05-23 05:37:29', '2021-05-23 05:37:29', 1, 4),
(20, 'Pasquier vous initie à la DRILL', 'https://www.youtube.com/watch?v=lbeUyW6axeA', 'Drill FR 4 THEMA LA KICHTA', '2021-05-23 07:17:04', '2021-05-23 07:17:04', 1, 3),
(21, 'MCD Conceptuel', '/database/mcd.pdf', 'Modèle Conceptuel de Données', '2021-05-23 13:11:14', '2021-05-23 13:11:14', 1, 1),
(22, 'MLD Logique', '/database/mld.pdf', 'Modèle Logique de Données', '2021-05-23 13:12:27', '2021-05-23 13:12:27', 1, 1),
(23, 'MPD Physique', '/database/gay_florian_info1c_ged_104_2021.sql', 'Modèle Physique de Données', '2021-05-23 13:14:44', '2021-05-23 13:14:44', 1, 1),
(25, 'Les tutos de Gunther', 'https://epsic.jacktrash.ch', 'Tutos de Julien pour les modules EPSIC 1ère année informaticien', '2021-05-24 08:25:58', '2021-05-24 08:25:58', 1, 2),
(26, 'Dictionnaire de données', '/database/dictionnaire.pdf', 'Dictionnaire de données module 104', '2021-05-24 08:30:48', '2021-05-24 08:30:48', 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `t_doc_have_cat`
--

CREATE TABLE `t_doc_have_cat` (
  `id_doc_have_cat` int(11) NOT NULL,
  `fk_doc` int(11) NOT NULL,
  `fk_cat` int(11) NOT NULL,
  `date_doctocat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_doc_have_cat`
--

INSERT INTO `t_doc_have_cat` (`id_doc_have_cat`, `fk_doc`, `fk_cat`, `date_doctocat`) VALUES
(5, 2, 1, '2021-04-28 06:15:13'),
(6, 3, 1, '2021-04-28 06:15:13'),
(7, 2, 3, '2021-05-05 14:07:29'),
(16, 10, 1, '2021-05-21 11:32:51'),
(17, 10, 3, '2021-05-21 11:32:51'),
(18, 7, 11, '2021-05-22 09:22:23'),
(19, 7, 4, '2021-05-22 09:22:23'),
(20, 8, 8, '2021-05-22 16:50:14'),
(21, 19, 5, '2021-05-23 05:37:55'),
(22, 20, 5, '2021-05-23 07:17:36'),
(23, 15, 1, '2021-05-23 12:59:06'),
(24, 16, 1, '2021-05-23 13:00:02'),
(25, 16, 3, '2021-05-23 13:00:02'),
(26, 18, 4, '2021-05-23 13:01:22'),
(27, 14, 10, '2021-05-23 13:02:21'),
(28, 14, 4, '2021-05-23 13:02:21'),
(29, 10, 8, '2021-05-23 13:03:13'),
(30, 21, 7, '2021-05-23 13:11:24'),
(31, 22, 7, '2021-05-23 13:12:43'),
(32, 23, 7, '2021-05-23 13:14:52'),
(33, 16, 10, '2021-05-23 13:16:32'),
(35, 25, 5, '2021-05-24 08:26:19'),
(36, 17, 10, '2021-05-24 08:29:36'),
(37, 26, 7, '2021-05-24 08:31:46'),
(39, 15, 10, '2021-05-24 08:33:41');

-- --------------------------------------------------------

--
-- Structure de la table `t_type`
--

CREATE TABLE `t_type` (
  `id_type` int(11) NOT NULL,
  `fileType` varchar(4) NOT NULL,
  `fileSignature` varchar(100) NOT NULL,
  `fileRGB` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_type`
--

INSERT INTO `t_type` (`id_type`, `fileType`, `fileSignature`, `fileRGB`) VALUES
(1, 'pdf', '25 50 44 46 2D', '255,99,132'),
(2, 'docx', '50 4B 03 04', '54,162,235'),
(3, 'xlsx', '50 4B 03 04', '75,192,192'),
(4, 'pptx', '50 4B 03 04', '255,159,64');

-- --------------------------------------------------------

--
-- Structure de la table `t_user`
--

CREATE TABLE `t_user` (
  `id_user` int(11) NOT NULL,
  `userName` varchar(40) NOT NULL,
  `userEmail` varchar(100) NOT NULL,
  `userPassword` varchar(130) NOT NULL,
  `lastLogin` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_user`
--

INSERT INTO `t_user` (`id_user`, `userName`, `userEmail`, `userPassword`, `lastLogin`) VALUES
(10, 'admin', 'admin@toto.psw', 'pbkdf2:sha256:150000$oxrxBhYi$afe1805ddb77d2fd50467d1c478ae363f1769b11ebb0f1d2c38fa9082b2ff020', '2021-05-08 07:28:25'),
(11, 'flow', 'me@fl0.me', 'pbkdf2:sha256:150000$0pt5rrNc$d5d60636f8699901bedf2961d9a804dc37f73703597cca2db4a09abec700b54c', '2021-05-11 06:23:56'),
(12, 'kneuss', 'kneu@ss.ch', 'pbkdf2:sha256:150000$VhIdspMX$74f1bb6a22399bd8e31234608b62c37bdf9421baaa96798f2bbfbd07edf7e5da', '2021-05-12 07:26:04'),
(15, 'hello', 'hello@hello.com', 'pbkdf2:sha256:150000$yBFCqVmk$53b586a2409c4cc04672415d7fd3e0f7e065e9fa44d774ff4acd2c898f76255e', '2021-06-01 09:11:06');

-- --------------------------------------------------------

--
-- Structure de la table `t_user_put_doc`
--

CREATE TABLE `t_user_put_doc` (
  `id_user_put_doc` int(11) NOT NULL,
  `fk_user` int(11) NOT NULL,
  `fk_doc` int(11) NOT NULL,
  `date_userputdoc` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `t_category`
--
ALTER TABLE `t_category`
  ADD PRIMARY KEY (`id_cat`);

--
-- Index pour la table `t_document`
--
ALTER TABLE `t_document`
  ADD PRIMARY KEY (`id_document`),
  ADD KEY `fk_type` (`fk_type`);

--
-- Index pour la table `t_doc_have_cat`
--
ALTER TABLE `t_doc_have_cat`
  ADD PRIMARY KEY (`id_doc_have_cat`),
  ADD KEY `fk_doc` (`fk_doc`),
  ADD KEY `fk_cat` (`fk_cat`);

--
-- Index pour la table `t_type`
--
ALTER TABLE `t_type`
  ADD PRIMARY KEY (`id_type`);

--
-- Index pour la table `t_user`
--
ALTER TABLE `t_user`
  ADD PRIMARY KEY (`id_user`),
  ADD UNIQUE KEY `userEmail` (`userEmail`),
  ADD UNIQUE KEY `userName` (`userName`);

--
-- Index pour la table `t_user_put_doc`
--
ALTER TABLE `t_user_put_doc`
  ADD PRIMARY KEY (`id_user_put_doc`),
  ADD KEY `fk_user` (`fk_user`),
  ADD KEY `fk_doc` (`fk_doc`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `t_category`
--
ALTER TABLE `t_category`
  MODIFY `id_cat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT pour la table `t_document`
--
ALTER TABLE `t_document`
  MODIFY `id_document` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT pour la table `t_doc_have_cat`
--
ALTER TABLE `t_doc_have_cat`
  MODIFY `id_doc_have_cat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT pour la table `t_type`
--
ALTER TABLE `t_type`
  MODIFY `id_type` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `t_user`
--
ALTER TABLE `t_user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT pour la table `t_user_put_doc`
--
ALTER TABLE `t_user_put_doc`
  MODIFY `id_user_put_doc` int(11) NOT NULL AUTO_INCREMENT;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `t_document`
--
ALTER TABLE `t_document`
  ADD CONSTRAINT `t_document_ibfk_1` FOREIGN KEY (`fk_type`) REFERENCES `t_type` (`id_type`);

--
-- Contraintes pour la table `t_doc_have_cat`
--
ALTER TABLE `t_doc_have_cat`
  ADD CONSTRAINT `t_doc_have_cat_ibfk_1` FOREIGN KEY (`fk_doc`) REFERENCES `t_document` (`id_document`),
  ADD CONSTRAINT `t_doc_have_cat_ibfk_2` FOREIGN KEY (`fk_cat`) REFERENCES `t_category` (`id_cat`);

--
-- Contraintes pour la table `t_user_put_doc`
--
ALTER TABLE `t_user_put_doc`
  ADD CONSTRAINT `t_user_put_doc_ibfk_1` FOREIGN KEY (`fk_user`) REFERENCES `t_user` (`id_user`),
  ADD CONSTRAINT `t_user_put_doc_ibfk_2` FOREIGN KEY (`fk_doc`) REFERENCES `t_document` (`id_document`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
