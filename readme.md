Gestion éléctronique de documents > GED🐼 
---

GedPanda est une gestion électronique de document développée en Python avec le micro framework Flask dans le cadre d'un module sur les bases de données pour le CFC de développeur d'application.

###### Visuel Frontend - GED partie utilisateurs
![Frontend GedPanda](APP_FILMS/database/screen_frontend.png "Frontend GedPanda")
###### Visuel Backend - Adminstration de la GED
![Backend GedPanda](APP_FILMS/database/screen_backend.png "Backend GedPanda")

La sécurité et l'intégrité de vos données n'est pas garantie. Il ne faut PAS utiliser GedPanda en production /!\ Il est possible de l'utiliser à des fins de test en local uniquement.
> **5 Pandas sont cachés dans ce logiciel, saurez-vous tous les retrouver ?**
# 🐼 GED Panda - v.Alpha 🌱
##### Prérequis pour l'installation / utilisation :
* Python 3.8 (ou supérieur)
* Uwamp avec les configurations par défault
* L'IDE PyCharm Education

##### Installation
1. Ouvrez PyCharm et cliquez sur le bouton Get from VCS (Version Control System)
2. Entrez l'adresse de ce repository Git ( https://gitlab.com/flowgrapher/ged ) et choisissez un dossier local
3. Vérifiez que Uwamp est bien lancé et que PhpmyAdmin est accessible
4. Configurez le fichier .env à la racine du projet avec les paramètres de votre serveur MySql
5. Dans le projet, ouvrez le dossier zzzdemos et faite un Run du fichier 1_ImportationDumpSql.py pour importer la base de données
6. Lancez ensuite le fichier 1_run_server_flask.py à la racine pour démarrez le projet
7. Cliquez sur l'URL de la console Run pour accéder à la GED (par défaut 127.0.0.1:5005)
8. Connection au panel d'administration login: admin, mot de passe: toto

##### Conception / Schémas BDD
* [Dictionnaire de données](https://gitlab.com/flowgrapher/ged/-/blob/master/APP_FILMS/database/dictionnaire.pdf)
* [Modèle Conceptuel de Données](https://gitlab.com/flowgrapher/ged/-/blob/master/APP_FILMS/database/mcd.pdf)
* [Modèle Logique de Données](https://gitlab.com/flowgrapher/ged/-/blob/master/APP_FILMS/database/mld.pdf)
* [Modèle Physique de Données](https://gitlab.com/flowgrapher/ged/-/blob/master/APP_FILMS/database/gay_florian_info1c_ged_104_2021.sql)
